.PHONY: all lib install clean

all: lib

lib:
	(cd IxFree; make all)

install:
	(cd IxFree; make install)

clean:
	(cd IxFree; make clean)
